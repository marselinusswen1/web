---
title: "Laporan Finansial"
subtitle: ""
# meta description
description: "Jazaakumullahu khairan katsiran kepada para donatur atas donasi terbaiknya."
image: media/beranda/thumbnail.jpg
opengraph:
  image: media/beranda/thumbnail.jpg
draft: true

call_to_action:
  enable : true
  title : "Butuh Bantuan?"
  image : "media/beranda/faq-min.svg"
  content : "Silakan kontak kami di telegram. Balasan akan direspon 1x3 jam."
  button:
    enable : true
    label : "Kabari via Telegram"
    link : "https://t.me/LangitKetujuh_bot"
---

## Total Donasi: Rp1,242,970.50

| **Tanggal**		| **Donatur**			| **Kredit**
|:---			| :---				| ---:
|10/11/2020 14:39 	| Seseorang			| Rp9,835.00
|12/11/2020 16:56 	| wardatul jannah		| Rp100,000.00
|16/11/2020 05:55 	| prawita			| Rp49,000.00
|24/11/2020 08:02 	| Habib				| Rp50,000.00
|25/11/2020 17:15 	| Muhamad Nur Halim		| Rp110,000.00
|08/12/2020 13:03 	| Yunus Ahmad			| Rp98,350.00
|19/12/2020 19:51 	| dimasnirwan			| Rp9,835.00
|22/12/2020 09:04 	| Yahya Sofyan			| Rp9,800.00
|28/12/2020 20:32 	| yogi				| Rp40,000.00
|06/01/2021 13:46 	| Adam R P			| Rp49,175.00
|10/01/2021 18:01 	| Seseorang			| Rp50,000.00
|12/01/2021 12:56 	| Dias Fahmi			| Rp9,800.00
|13/01/2021 19:28 	| Seseorang			| Rp49,000.00
|14/01/2021 06:50 	| Seseorang			| Rp98,000.00
|15/01/2021 14:39 	| Seseorang			| Rp50,000.00
|18/01/2021 08:04 	| Awal Syarman			| Rp49,175.00
|19/01/2021 17:34 	| Seseorang			| Rp70,000.00
|22/01/2021 08:34 	| Yunus ahmad			| Rp49,000.00
|02/02/2021 16:03 	| Mohammad Qowaidul Umam	| Rp50,000.00
|05/02/2021 16:16 	| Rikki Ahmad Ibrahim		| Rp49,175.00
|10/02/2021 09:43 	| Ihsan Syauqi Adn		| Rp9,835.00
|12/02/2021 10:21 	| Seseorang			| Rp30,000.00
|15/02/2021 06:02 	| Seseorang			| Rp9,835.00
|18/02/2021 10:14 	| Hamba Allah			| Rp30,000.00
|11/03/2021 13:51 	| Abu Zaid Tutorial		| Rp50,000.00
|11/03/2021 14:55 	| Hamba Allah			| Rp50,000.00
|11/03/2021 18:56 	| Seseorang			| Rp49,175.00
|13/03/2021 04:44 	| Seseorang			| Rp29,400.00
|			|**- PPN 5%**			| **Rp65,419.50**
|			|**Total**			| **Rp1,242,970.50**
