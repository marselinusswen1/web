---
title: "Terima Kasih"
subtitle: ""
# meta description
description: "Formulir Kerjasama."
image: media/beranda/thumbnail.jpg
opengraph:
  image: media/beranda/thumbnail.jpg
draft: false

call_to_action:
  enable : true
  title : "Artikel Menarik!"
  image : "media/beranda/pro-min.svg"
  content : "Yuk ikuti konten kreatif dari kami."
  button:
    enable : true
    label : "Blog"
    link : "/blog"
---

Anda sudah mengisi formulir halaman Kerjasama. Kami akan mengevalusi dan mengabari segera. ^^

_Syukran wa jazaakumullahu khairan katsiran._
