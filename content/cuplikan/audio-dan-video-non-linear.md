---
title: "Audio dan Video Non-linear"
date: 2020-12-12T23:23:19+07:00
image: media/cuplikan/audio-dan-video-non-linear.jpg
opengraph:
  image: media/cuplikan/audio-dan-video-non-linear.jpg
description: "Daftar cuplikan perangkat lunak Audio dan Video Non-linear di LangitKetujuh OS"
author: "LangitKetujuh ID"
draft: false
---

## Kdenlive (Video editor)

Kdenlive merupakan salah satu pengolah video terbaik berbasis GNU/linux. Kdenlive didukung oleh KDE dengan fitur yang melimpah.

{{< figure src=/media/cuplikan/kdenlive-langitketujuh-id-1.webp alt="Kdenlive LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/kdenlive-langitketujuh-id-2.webp alt="Kdenlive LangitKetujuh OS">}}

## Audacity (Audio Editor)

Audacity merupakan salah satu aplikasi pemberi efek suara, mengoreksi berkas suara tertentu, atau sekedar menambahkan berbagai efek yang disediakan.

{{< figure src=/media/cuplikan/audacity-langitketujuh-id-1.webp alt="Audacity LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/audacity-langitketujuh-id-2.webp alt="Audacity LangitKetujuh OS">}}

## Ardour (DAW)

{{< figure src=/media/cuplikan/ardour-langitketujuh-id-1.webp alt="Ardour LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/ardour-langitketujuh-id-2.webp alt="Ardour LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/ardour-langitketujuh-id-3.webp alt="Ardour LangitKetujuh OS">}}

## OBS Studio (Perekam Profesional)

{{< figure src=/media/cuplikan/obs-recorder-langitketujuh-id-1.webp alt="OBS Studio LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/obs-recorder-langitketujuh-id-2.webp alt="OBS Studio LangitKetujuh OS">}}

## Simple Screen Recorder (Perekam Sederhana)

{{< figure src=/media/cuplikan/simplescreenrecorder-langitketujuh-id-1.webp alt="Simplescreenrecorder LangitKetujuh OS">}}

## SoundKonverter (Konversi Audio)

{{< figure src=/media/cuplikan/soundkonverter-langitketujuh-id-1.webp alt="SoundKonverter LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/soundkonverter-langitketujuh-id-2.webp alt="SoundKonverter LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/soundkonverter-langitketujuh-id-3.webp alt="SoundKonverter LangitKetujuh OS">}}
