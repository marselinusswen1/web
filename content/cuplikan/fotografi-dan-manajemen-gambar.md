---
title: "Fotografi dan Manajemen Gambar"
date: 2020-12-12T23:23:17+07:00
image: media/cuplikan/fotografi-dan-manajemen-gambar.jpg
opengraph:
  image: media/cuplikan/fotografi-dan-manajemen-gambar.jpg
description: "Daftar cuplikan perangkat lunak Fotografi dan Manajemen Gambar di LangitKetujuh OS"
author: "LangitKetujuh ID"
draft: false
---

## RawTherapee (Penyunting Foto RAW)

RawTherapee adalah editor foto yang secara khusus didesain untuk berkas RAW. Perangkat lunak ini kompatibel dengan format kamera DSLR (seperti NEF dan DNG), dan juga mendukung format tradisional seperti JPG dan TIFF.

{{< figure src=/media/cuplikan/rawtherapee-langitketujuh-id-1.webp alt="RawTherapee LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/rawtherapee-langitketujuh-id-2.webp alt="RawTherapee LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/rawtherapee-langitketujuh-id-3.webp alt="RawTherapee LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/rawtherapee-langitketujuh-id-4.webp alt="RawTherapee LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/rawtherapee-langitketujuh-id-5.webp alt="RawTherapee LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/rawtherapee-langitketujuh-id-6.webp alt="RawTherapee LangitKetujuh OS">}}

## Digikam (Manajemen Foto)

Aplikasi bersumber terbuka yang didesain untuk mengatur, mengedit, dan melihat seluruh koleksi gambar. Perangkat lunak ini mampu membuat album dengan berbagai macam penanda (tagging), menjalankan pencarian lanjutan untuk gambar berdasarkan berbagai detail (tag, tanggal, lokasi, EXIF, IPTC, XMP, dsb) dan membuat katalog video.

{{< figure src=/media/cuplikan/digikam-langitketujuh-id-1.webp alt="Digikam LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/digikam-langitketujuh-id-2.webp alt="Digikam LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/digikam-langitketujuh-id-3.webp alt="Digikam LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/digikam-langitketujuh-id-4.webp alt="Digikam LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/digikam-langitketujuh-id-5.webp alt="Digikam LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/digikam-langitketujuh-id-6.webp alt="Digikam LangitKetujuh OS">}}
