---
title: "Aneka Perlengkapan Perkantoran"
date: 2020-12-12T23:23:16+07:00
image: media/cuplikan/perlengkapan-perkantoran.jpg
opengraph:
  image: media/cuplikan/perlengkapan-perkantoran.jpg
description: "Daftar cuplikan perangkat lunak Aneka Produktifitas Keseharian di LangitKetujuh OS"
author: "LangitKetujuh ID"
draft: false
---

LibreOffice merupakan paket perangkat lunak bebas untuk perlengkapan perkantoran yang lengkap. Mulai dari untuk membuat laporan, makalah, skripsi, disertasi, tesis. Bahkan mampu membuat database sendiri. MasyaAllah, lengkap sekali.

{{< figure src=/media/cuplikan/libreoffice-langitketujuh-id-2.webp alt="LibreOffice Draw LangitKetujuh OS">}}

## LibreOffice Writer

LibreOffice Writer digunakan untuk penulisan doumen berbasis teks. Seperti membuat makalah, laporan, dan sebagainya.

{{< figure src=/media/cuplikan/libreoffice-langitketujuh-id-3.webp alt="LibreOffice Draw LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/libreoffice-langitketujuh-id-4.webp alt="LibreOffice Draw LangitKetujuh OS">}}

## LibreOffice Calc

Perangkat lunak ini digunakan untuk membuat dokumen berbasis angka.

{{< figure src=/media/cuplikan/libreoffice-langitketujuh-id-5.webp alt="LibreOffice Draw LangitKetujuh OS">}}

## LibreOffice Impress

Membuat dokumen untuk kebutuhan presentasi.

{{< figure src=/media/cuplikan/libreoffice-langitketujuh-id-6.webp alt="LibreOffice Draw LangitKetujuh OS">}}

## LibreOffice Draw

Membuat dokumen untuk kebutuhan gambar berbasis vektor. LibreOffice Draw juga mampu dijadikan sebagai Penyunting PDF yang handal.

{{< figure src=/media/cuplikan/libreoffice-langitketujuh-id-7.webp alt="LibreOffice Draw LangitKetujuh OS">}}

## LibreOffice Math

Membuat teks formula matematika dengan mudah dilakukan dengan LibreOffice Formula.

{{< figure src=/media/cuplikan/libreoffice-langitketujuh-id-8.webp alt="LibreOffice Draw LangitKetujuh OS">}}

## LibreOffice Base

Membuat bibliografi, data perpustakaan, data penjualan mudah dibuat dengan LibreOffice Base. Jika mau menanamkan database bisa menggunakan Fiebird3 sebagai database utamanya.

{{< figure src=/media/cuplikan/libreoffice-langitketujuh-id-9.webp alt="LibreOffice Draw LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/libreoffice-langitketujuh-id-10.webp alt="LibreOffice Draw LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/libreoffice-langitketujuh-id-11.webp alt="LibreOffice Draw LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/libreoffice-langitketujuh-id-12.webp alt="LibreOffice Draw LangitKetujuh OS">}}

## Okular (Pembuka PDF)

Okular merupakan pembuka PDF yang ringan dan bebas. Okular juga berada dibawah naungan organisasi non-profit KDE e.V.

{{< figure src=/media/cuplikan/okular-langitketujuh-id-1.webp alt="Okular LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/okular-langitketujuh-id-2.webp alt="Okular LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/okular-langitketujuh-id-3.webp alt="Okular LangitKetujuh OS">}}
