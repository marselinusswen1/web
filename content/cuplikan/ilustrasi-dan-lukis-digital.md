---
title: "Ilustrasi dan Lukis Digital"
date: 2020-12-12T23:23:23+07:00
image: media/cuplikan/ilustrasi-vektor-dan-pelukis-digital.jpg
opengraph:
  image: media/cuplikan/ilustrasi-vektor-dan-pelukis-digital.jpg
description: "Daftar cuplikan perangkat lunak Ilustrasi Vektor dan Digital Painting di LangitKetujuh OS"
author: "LangitKetujuh ID"
draft: false
---

## Inkscape (Vektor editor)

Inkscape merupakan perangkat lunak pengolah vektor, digunakan untuk membuat logo, ilustrasi, pattern dan lain-lain.

{{< figure src=/media/cuplikan/inkscape-langitketujuh-id-1.webp alt="Inkscape Version LangitKetujuh OS">}}

{{< figure src=/media/cuplikan/inkscape-langitketujuh-id-2.webp alt="Inkscape Workspace LangitKetujuh OS">}}

## Krita (Lukis digital)

Krita adalah perangkat lunak bebas dan sumber terbuka untuk editor grafis raster, yang dirancang terutama untuk menggambar, melukis dan animasi.

{{< figure src=/media/cuplikan/krita-langitketujuh-id-1.webp alt="Krita LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/krita-langitketujuh-id-2.webp alt="Krita LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/krita-langitketujuh-id-3.webp alt="Krita LangitKetujuh OS">}}

## Gimp (Manipulasi Foto)

Gimp sebagai raster foto editor, pembuat kover buku dan majalah.


{{< figure src=/media/cuplikan/gimp-langitketujuh-id-1.webp alt="Gimp LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/gimp-langitketujuh-id-2.webp alt="Gimp LangitKetujuh OS">}}

## GMIC QT (Plugin Gimp dan Krita)

GMIC merupakan plugin tambahan untuk memberikan efek pada foto. GMIC plugin juga tersedia untuk gimp dan krita.

Tampilan GMIC Gimp:

{{< figure src=/media/cuplikan/gmic-gimp-langitketujuh-id.webp alt="GMIC Gimp LangitKetujuh OS">}}

Tampilan GMIC Krita:

{{< figure src=/media/cuplikan/gmic-krita-langitketujuh-id.webp alt="GMIC Krita LangitKetujuh OS">}}
