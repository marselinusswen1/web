---
title: "Mesin Game 2D/3D"
date: 2020-12-12T23:23:20+07:00
image: media/cuplikan/mesin-game-multi-platform-2d-3d.jpg
opengraph:
  image: media/cuplikan/mesin-game-multi-platform-2d-3d.jpg
description: "Daftar cuplikan perangkat lunak Multi Platform Kreator 2D/3D Mesin Game di LangitKetujuh OS"
author: "LangitKetujuh ID"
draft: false
parent: cuplikan
---

Godot Game Engine merupakan multi platform game engine yang memiliki fitur untuk membuat permainan 2D dan 3D. Perangkat lunak ini menyediakan banyak tools umum yang komprehensif. Sehingga dapat memudahkan pengguna dalam mengembangkan sebuah game.

Fitur Multi Platform dapat mengeksport ke beberapa platform desktop seperti Windows, Linux, ataupun MacOS, serta platform mobile seperti Android dan IOS, dan berbasis web seperti HTML 5.

{{< figure src=/media/cuplikan/godot-langitketujuh-id-1.webp alt="Godot LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/godot-langitketujuh-id-2.webp alt="Godot LangitKetujuh OS">}}

## Godot 2D Demo

Cuplikan salah satu demo 2D scenes dari Godot.

{{< figure src=/media/cuplikan/godot-langitketujuh-id-3.webp alt="Godot LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/godot-langitketujuh-id-4.webp alt="Godot LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/godot-langitketujuh-id-5.webp alt="Godot LangitKetujuh OS">}}

## Godot 3D Third Person Shooter Demo

Dengan Godot juga mampu membuat game 3D yang detail.

{{< figure src=/media/cuplikan/godot-langitketujuh-id-6.webp alt="Godot LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/godot-langitketujuh-id-7.webp alt="Godot LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/godot-langitketujuh-id-8.webp alt="Godot LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/godot-langitketujuh-id-9.webp alt="Godot LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/godot-langitketujuh-id-10.webp alt="Godot LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/godot-langitketujuh-id-11.webp alt="Godot LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/godot-langitketujuh-id-12.webp alt="Godot LangitKetujuh OS">}}
