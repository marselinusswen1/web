---
title: "Kreasi Animasi 2D/3D Full Fitur"
date: 2020-12-12T23:23:22+07:00
image: media/cuplikan/animasi-2d-3d-penuh-fitur.jpg
opengraph:
  image: media/cuplikan/animasi-2d-3d-penuh-fitur.jpg
description: "Daftar cuplikan perangkat lunak Kreasi Animasi 2D/3D Full Fitur di LangitKetujuh OS"
author: "LangitKetujuh ID"
draft: false
---

## Blender (Animasi 3D)

Blender 3D sebagai perangkat lunak animasi 3D dan 2D.

{{< figure src=/media/cuplikan/blender-3d-langitketujuh-id-1.webp alt="Blender 3D LangitKetujuh OS">}}

## Synfigstudio (Animasi 2D vektor dan raster)

SynfigStudio merupakan pembuat animasi dari vektor dan bitmap, tanpa membuat animasi frame by frame.

{{< figure src=/media/cuplikan/synfigstudio-langitketujuh-id-1.webp alt="Synfigstudio LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/synfigstudio-langitketujuh-id-2.webp alt="Synfigstudio LangitKetujuh OS">}}

## Opentoonz (Animasi 2D Raster)

{{< figure src=/media/cuplikan/opentoonz-langitketujuh-id-1.webp alt="Opentoonz LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/opentoonz-langitketujuh-id-2.webp alt="Opentoonz LangitKetujuh OS">}}
