---
title: "Desktop Publishing"
date: 2020-12-12T23:23:18+07:00
image: media/cuplikan/open-source-desktop-publishing.jpg
opengraph:
  image: media/cuplikan/open-source-desktop-publishing.jpg
description: "Daftar cuplikan perangkat lunak Open Source Desktop Publishing di LangitKetujuh OS"
author: "LangitKetujuh ID"
draft: false
---

## Scribus (Desktop Publishing)

Scribus adalah penerbitan aplikasi (DTP) desktop, dirilis di bawah GNU General Public License sebagai perangkat lunak bebas.

Perangkat lunak Scribus dirancang untuk kebutuhan tata letak, huruf dan mempersiapkan berkas untuk kualitas gambar secara profesional. Dengan ini juga dapat membuat presentasi PDF. Contoh penggunaan scribus yaitu seperti membuat koran, brosur, buletin, poster, majalah dan buku.

{{< figure src=/media/cuplikan/scribus-langitketujuh-id-1.webp alt="Scribus LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/scribus-langitketujuh-id-2.webp alt="Scribus LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/scribus-langitketujuh-id-3.webp alt="Scribus LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/scribus-langitketujuh-id-4.webp alt="Scribus LangitKetujuh OS">}}

## LibreOffice Draw (PDF Editor)

LibreOffice Draw merupakan komponen aplikasi LibreOffice yang berfungsi untuk membuat dan memanipulasi data gambar digital 2 dimensi. Bentuk gambar vektor standar seperti garis, lingkaran, kotak, persegi, segitiga, busur dan bangun geometri lainnya dapat dibuat dengan mudah dan cepat.

Hampir semua format gambar digital yang umum digunakan dalam aplikasi komputer bisa dibuat dan dimanipulasi dengan program ini. Selain itu aplikasi LibreOffice Draw juga dapat digunakan untuk kebutuhan editing Postscript dan PDF.

{{< figure src=/media/cuplikan/libreoffice-langitketujuh-id-2.webp alt="LibreOffice Draw LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/libreoffice-langitketujuh-id-7.webp alt="LibreOffice Draw LangitKetujuh OS">}}
