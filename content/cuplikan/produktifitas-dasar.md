---
title: "Produktifitas Dasar"
date: 2020-12-12T23:23:16+07:00
image: media/cuplikan/produktifitas-dasar.jpg
opengraph:
  image: media/cuplikan/produktifitas-dasar.jpg
description: "Daftar cuplikan perangkat lunak Produktifitas Dasar di LangitKetujuh OS"
author: "LangitKetujuh ID"
draft: false
---

## Start Menu

Tampilan start menu dari LangitKetujuh PRO yang lengkap untuk kebutuhan desain.

{{< figure src=/media/cuplikan/start-menu-langitketujuh-id.webp alt="Start Menu LangitKetujuh OS">}}

## Ksysguard (Sistem Monitor)

Penggunaan daya RAM yang minimal. Tidak lebih dari 400mb yang digunakan.

{{< figure src=/media/cuplikan/ksysguard-langitketujuh-id-1.webp alt="Ksysguard LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/ksysguard-langitketujuh-id-2.webp alt="Ksysguard LangitKetujuh OS">}}

## Firefox (Web Browser)

Peselancar situs bawaan menggunakan Perangkat lunak Firefox.

{{< figure src=/media/cuplikan/firefox-langitketujuh-id-1.webp alt="Firefox LangitKetujuh OS">}}

## Color Picker

Dipanel sudah ada widget color picker untuk memudahkan mengambil warna.

{{< figure src=/media/cuplikan/color-picker-langitketujuh-id-1.webp alt="Color Picker LangitKetujuh OS">}}

## Dolphin (File Manager)

Pembuka manajer berkas menggunakan dolphin bawaan dari KDE.

{{< figure src=/media/cuplikan/dolphin-langitketujuh-id-1.webp alt="Dolphin LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/dolphin-langitketujuh-id-2.webp alt="Dolphin LangitKetujuh OS">}}

## Elisa (Pemutar Audio)

Pemutar audio menggunakan elisa karena mudah diintegrasikan dan dibawah naungan KDE.

{{< figure src=/media/cuplikan/elisa-langitketujuh-id-1.webp alt="Elisa LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/elisa-langitketujuh-id-2.webp alt="Elisa LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/elisa-langitketujuh-id-3.webp alt="Elisa LangitKetujuh OS">}}
