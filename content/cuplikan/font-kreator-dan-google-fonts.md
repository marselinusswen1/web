---
title: "Font Kreator dan Google Fonts"
date: 2020-12-12T23:23:21+07:00
image: media/cuplikan/font-kreator-dan-google-fonts.jpg
opengraph:
  image: media/cuplikan/font-kreator-dan-google-fonts.jpg
description: "Daftar cuplikan Google Fonts dan Pembuat Truetype Font di LangitKetujuh OS"
author: "LangitKetujuh ID"
draft: false
---

## FontForge (Pembuat Truetype Font)

FontForge adalah aplikasi gratis yang digunakan untuk membuat, mengedit, dan mengonversi berbagai format font. Mendukung format font populer, seperti font TrueType, Bitmap, dan PostScript.

FontForge Editor tersedia untuk Windows, OS X, dan Linux. Tentunya di LangitKetujuh OS PRO, FontForge sudah terpasang otomatis.

{{< figure src=/media/cuplikan/fontforge-langitketujuh-id-1.webp alt="FontForge LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/fontforge-langitketujuh-id-2.webp alt="FontForge LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/fontforge-langitketujuh-id-3.webp alt="FontForge LangitKetujuh OS">}}

## Google Fonts (Open Font)

Google font merupakan font yang berlisensi OFL (Open Font License) yang mengutamakan performa untuk memuat font di browser. Biasanya digunakan oleh web desainer, ui/ix, dan tipografi ilustrator. Di LangitKetujuh Pro sudah terinstall Google Fonts dengan jumlah sekitar 1200+ font.

{{< figure src=/media/cuplikan/google-fonts-langitketujuh-id-1.webp alt="Google Fonts LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/google-fonts-langitketujuh-id-2.webp alt="Google Fonts LangitKetujuh OS">}}
{{< figure src=/media/cuplikan/google-fonts-langitketujuh-id-3.webp alt="Google Fonts LangitKetujuh OS">}}
