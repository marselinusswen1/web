---
title: "Peraturan Layanan"
subtitle: ""
# meta description
description: "Hak dan kewajiban tim LangitKetujuh dan Anda."
image: media/beranda/thumbnail.jpg
opengraph:
  image: media/beranda/thumbnail.jpg
draft: false
---

### Kami

Kami sebagai tim LangitKetujuh ID dan pemilik situs https://langitketujuh.id yang memperkenalkan sistem operasi dan produk yang dibuat dengan FLOSS (Free/Libre Open Source Software). LangitKetujuh ID memperhatikan privasi Anda dengan sangat serius. Kebijakan privasi ini menggambarkan informasi pribadi yang kami kumpulkan dan bagaimana kami menggunakannya data tersebut.

### Pengguna

Pengguna merupakan individu/lembaga yang menggunakan dan/atau sudah memasang sistem operasi LangitKetujuh.

### Perihal Donasi

Pengguna profesional dengan dukungan layanan memberikan donasi mulai dari Rp107.000,00 apabila pemasangan tidak berhasil, maka donasi bisa dikembalikan. Atau kami salurkan ke smart171.org dengan potongan sebesar Rp7000.

Pengguna profesional yang memberikan donasi mulai dari Rp57.000,00 apabila pemasangan tidak berhasil, maka donasi akan menjadi milik LangitKetujuh ID dan tidak bisa dikembalikan, namun donasi akan tetap diberikan dengan jumlah 20% dari total donasi.

Donasi 20% dari total akan kami salurkan ke https://smart171.org untuk kebutuhan Yatim di Gaza.

### Prioritas dan batasan pengguna

  * Pengguna pro yang telah berdonasi memiliki prioritas utama.
  * Pengguna telah mengetahui kebutuhan perangkat lunak baik secara personal atau perusahaan.
  * Pengguna telah mengetahui glibc dan setuju menggunakan bila memilihnya.
  * Pengguna telah mengetahui musl dan setuju menggunakan bila memilihnya.
  * Pengguna telah mengetahui musl dan menggunakan nouveau driver sebagai alternatif kartu grafis NVIDIA. Pengguna setuju menggunakan bila memilihnya.
  * Pertanyaan yang diajukan oleh pengguna lite dan pro hanya untuk yang berhubungan dengan langitketujuh OS dan seputar desain grafis.
  * Pengguna telah bersedia untuk memasang LangitKetujuh OS dengan single boot.

### Teknis layanan pemasangan sistem operasi

  * Kami TIDAK menerima untuk pemasangan dual boot, triple boot, quarter boot, dan seterusnya.
  * Kami TIDAK menerima untuk mengembalikan OS terdahulu. Baik itu windows, osx, GNU/Linux, BSD, dan jenis OS lainnya.
  * Kami TIDAK bertanggung jawab atas kehilangan data dan sistem operasi terdahulu.
  * Kami hanya menerima instalasi untuk satu perangkat dan **satu pemasangan saja**.
  * Kami hanya melayani untuk memasang LangitKetujuh OS, bukan memasang sistem operasi selain LangitKetujuh OS.
  * Kami hanya menggunakan partisi fat32 untuk partisi boot (`/boot` atau `/boot/efi`), xfs untuk sistem root (`/`) dan ext4 untuk partisi pengguna (`/home`).
