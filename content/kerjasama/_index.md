---
title: "Kerjasama"
subtitle: ""
# meta description
description: "Formulir Kerjasama untuk media partner, kolaborator dan sponsorship."
image: media/beranda/thumbnail.jpg
opengraph:
  image: media/beranda/thumbnail.jpg
draft: false
---

### Butuh bantuan?
Terima kasih telah mengisi formulir kerjasama kepada tim Langitketujuh OS. Anda juga dapat menghubungi tim L7 melalui aplikasi telegram dengan tautan dibawah ini. Syukran.

* Layanan: [@LangitKetujuh_bot](https://t.me/LangitKetujuh_bot)
* Kanal: [@langitketujuhID](https://t.me/langitketujuhID)
* Surel: [langitketujuh.id@pm.me](mailto:langitketujuh.id@pm.me)
