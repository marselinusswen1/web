---
title: "Komparasi Manjaro-X Design Suite dengan LangitKetujuh Pro"
date: 2020-12-12T23:23:22+07:00
image: media/komparasi/vs-manjaro-x-design-suite.webp
opengraph:
  image: media/komparasi/vs-manjaro-x-design-suite.webp
description: "Kelebihan dan kekurangan  Manjaro-X Design Suite dengan LangitKetujuh Pro."
author: "LangitKetujuh ID"
draft: false
---

### Dukungan Instalasi

**Fitur** | **Manjaro-X DS** | **L7 Pro Musl** | **L7 Pro Glibc**
:--- | :---: | :---: | :---:
Dukungan instalasi oleh tenaga ahli. | - | **√** | **√**

### Dukungan Musl Libc dan Non-free

**Fitur** | **Manjaro-X DS** | **L7 Pro Musl** | **L7 Pro Glibc**
:--- | :---: | :---: | :---:
Tersedia Musl Libc. | - | **√** | -
Non-SystemD. | - | **√** | **√**
Aplikasi Terbaru (Rilis bergulir). | **√** | **√** | **√**
Dukungan Aplikasi Appimage | **√** | - | **√**
Dukungan Nvidia Propietary driver | **√** | - | **√**
Dukungan Aplikasi Nonfree  | **√** |- | **√**
Dukungan Wine windows | **√** | - | **√**

### Dukungan Printer

**Fitur** | **Manjaro-X DS** | **L7 Pro Musl** | **L7 Pro Glibc**
:--- | :---: | :---: | :---:
Deteksi Printer Epson. | **√** | **√** | **√**
Deteksi Printer HP. | - | **√** | **√**

### Ilustrasi dan Digital Painting

**Fitur** | **Manjaro-X DS** | **L7 Pro Musl** | **L7 Pro Glibc**
:--- | :---: | :---: | :---:
Gimp manipulasi foto. | **√** | **√** | **√**
Inkscape pengolah vektor. | **√** | **√** | **√**
Krita digital painting. | **√** | **√** | **√**
Plugin GMIC Qt Gimp dan Krita. | - | **√** | **√**
Piko Pixel. | - | - | - 

### Kreasi Animasi 2D/3D Full **Fitur**

**Fitur** | **Manjaro-X DS** | **L7 Pro Musl** | **L7 Pro Glibc**
:--- | :---: | :---: | :---:
Blender animasi 2D/3D. | **√** | **√** | **√**
Opentoonz 2D. | - | **√** | **√**
Synfigstudio animasi 2D. | - | **√** | **√**

### Font Kreator dan Google Fonts

**Fitur** | **Manjaro-X DS** | **L7 Pro Musl** | **L7 Pro Glibc**
:--- | :---: | :---: | :---:
Fontforge pembuat font. | **√** | **√** | **√**
Google Fonts untuk proyek komersil. | - | **√** | **√**

### Mesin Game 2D/3D

**Fitur** | **Manjaro-X DS** | **L7 Pro Musl** | **L7 Pro Glibc**
:--- | :---: | :---: | :---:
Godot mesin game. | - | **√** | **√**

### Desktop Publishing

**Fitur** | **Manjaro-X DS** | **L7 Pro Musl** | **L7 Pro Glibc**
:--- | :---: | :---: | :---:
Scribus Desktop Publisher. | **√** | **√** | **√**
LibreOffice Draw | - | **√** | **√**
Calibre publisher. | - | - | -

### Fotografi dan Manajemen Gambar

**Fitur** | **Manjaro-X DS** | **L7 Pro Musl** | **L7 Pro Glibc**
:--- | :---: | :---: | :---:
Hugin Panorama. | - | **√** | **√**
Digikam manajemen foto. | **√** | **√** | **√**
Rawtherapee pengolah foto raw. | -| **√** | **√**
Darktable manajemen foto. | **√** | - | -
Shotwell manajemen foto. | - | - | - 

### Audio dan Video Non-linear

**Fitur** | **Manjaro-X DS** | **L7 Pro Musl** | **L7 Pro Glibc**
:--- | :---: | :---: | :---:
Dukungan codec FFmpeg, Audio dan Video. | **√** | **√** | **√**
OBS Open Broadcast. | **√** | **√** | **√**
Audacity pengolah audio. | **√** | **√** | **√**
Produksi Audio dengan Ardour DAW. | **√** | **√** | **√**
Produksi Audio dengan Cadence Jack. | - | **√** | **√**
SoundKonverter audio. | - | **√** | **√**
Kdenlive pengolah video. | **√** | **√** | **√**
Openshot pengolah video. | - | - | -

### Perlengkapan Perkantoran

**Fitur** | **Manjaro-X DS** | **L7 Pro Musl** | **L7 Pro Glibc**
:--- | :---: | :---: | :---:
LibreOffice Paket Aplikasi Perkantoran. | **√** | **√** | **√**

### Produktifitas Dasar

**Fitur** | **Manjaro-X DS** | **L7 Pro Musl** | **L7 Pro Glibc**
:--- | :---: | :---: | :---:
Firefox Web Browser. | **√** | **√** | **√**
Elisa Pemutar Audio. | **√** | **√** | **√**
Kaffeine Pemutar Video. | **√** | **√** | **√**
