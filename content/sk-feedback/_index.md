---
title: "Terima Kasih"
subtitle: ""
# meta description
description: "Formulir saran dan kritikan untuk pengembangan Langitketujuh OS."
image: media/beranda/thumbnail.jpg
opengraph:
  image: media/beranda/thumbnail.jpg
draft: false

call_to_action:
  enable : true
  title : "Artikel Menarik!"
  image : "media/beranda/pro-min.svg"
  content : "Yuk ikuti konten kreatif dari kami."
  button:
    enable : true
    label : "Blog"
    link : "/blog"
---

Anda sudah mengisi formulir halaman Feedback.

_Syukran wa jazaakumullahu khairan katsiran._
