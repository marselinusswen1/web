---
title: Tentang Kami
subtitle: ''
description: Tim LangitKetujuh beserta jajarannya.
image: media/beranda/thumbnail.jpg
opengraph:
  image: media/beranda/thumbnail.jpg
call_to_action:
  enable: true
  title: Butuh Bantuan?
  image: media/beranda/faq-min.svg
  content: Silakan kontak kami di telegram. Balasan akan direspon 1x3 jam.
  button:
    enable: true
    label: Kabari via Telegram
    link: https://t.me/LangitKetujuh_bot

---
### Inspirasi?

Amar ma'ruf nahi munkar. Mengajak kepada kebaikan, mencegah keburukan (kejahatan).

### Asal Nama LangitKetujuh?

Diambil dari peristiwa Isra' Miraj. Perjalanan Rasulullah dari Al Masjidil Haram ke Al Masjidil Aqsha. Mi'raj ke langit pertama, langit ketujuh, hingga ke _Sidratul Muntaha_. Tempat tertinggi di langit yang menjadi batas ujung pengetahuan dan amal aktifitas para makhluk.

Alasan kedua, nama LangitKetujuh ini diambil karena jarang ada projek free software yang menggunakan nama tempat yang islami dan istimewa, sehingga developer bertambah yakin dengan nama tersebut. Kita sebut saja L7, kepanjangan dari LangitKetujuh.

### Mengapa KDE?

Berbagai aplikasi desain kebanyakan terbuat dari QT. Seperti krita, kdenlive, scribus, fontforge, synfigstudio, opentoonz. Namun aplikasi desain gtk juga ada seperti Inkscape dan Gimp. Terlebih lagi tampilan UI KDE lebih familiar oleh pengguna awam.

### Mengapa untuk desainer saja?

Karena developer L7 juga desainer, pengajar desain grafis, freelancer, dan memiliki bakat di desain grafis. Meskipun demikian, L7 terdapat versi Lite yang digunakan untuk kebutuhan umum dan awam.

### Siapa Para Tim?

* Hervy Qurrotul Ainur Rozi (Developer & Desainer)
* Ihsan Syauqi Adn (Administrasi & Pendukung layanan)
* Jauzaa Wahyu Prabaswara (Pendukung layanan)
* (Selanjutnya bisa jadi Anda)