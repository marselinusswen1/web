---
title: "Terima Kasih"
subtitle: ""
# meta description
description: "Unduh PRO untuk kebutuhan profesional."
image: media/beranda/thumbnail.jpg
opengraph:
  image: media/beranda/thumbnail.jpg
draft: false

call_to_action:
  enable : true
  title : "Butuh Bantuan?"
  image : "media/beranda/faq-min.svg"
  content : "Silakan kontak kami di telegram. Balasan akan direspon 1x3 jam."
  button:
    enable : true
    label : "Kabari via Telegram"
    link : "https://t.me/LangitKetujuh_bot"
---

Anda sudah mengisi formulir.
Kami akan mengirim tautan unduhan melalui E-mail pada jam kerja (08.00-17.00).

_Syukran wa jazaakumullahu khairan katsiran._
