---
title: "Kebijakan Privasi"
subtitle: ""
# meta description
description: "Anda mungkin akan diminta untuk memberikan informasi pribadi yang berhubungan dengan Layanan L7, tujuannya untuk kemudahan menghubungi dan mengenali."
image: media/beranda/thumbnail.jpg
opengraph:
  image: media/beranda/thumbnail.jpg
draft: false
---

### Kami

Kami sebagai tim LangitKetujuh ID dan pemilik situs https://langitketujuh.id yang memperkenalkan sistem operasi dan produk yang dibuat dengan FLOSS (Free/Libre Open Source Software). LangitKetujuh ID memperhatikan privasi Anda dengan sangat serius. Kebijakan privasi ini menggambarkan informasi pribadi yang kami kumpulkan dan bagaimana kami menggunakannya data tersebut.

### Pengguna

Pengguna merupakan individu/lembaga yang menggunakan dan/atau sudah memasang sistem operasi LangitKetujuh.

### Data pribadi yang dikumpulkan

Server web kami menggunakan layanan CI/CD [Netlify](https://netlify.com) untuk melacak informasi dasar mengenai pengunjung sebuah website. Informasi ini mencakup, namun tidak terbatas pada, alamat IP, browser, dan waktu. Tak satu pun dari informasi ini secara pribadi bisa mengidentifikasi pengunjung spesifik ke situs ini. Informasi yang dilacak untuk keperluan administrasi rutin dan juga tujuan perawatan website.

  ##### Formulir

  Kami mengumpulkan data dengan layanan CI/CD [Netlify](https://netlify.com) yang ditampilkan di halaman formulir [pro-musl](../pro-musl), [pro-glibc](../pro-glibc), [kabari](../kabari), [feedback](../feedback), [kerjasama](../kerjasama) dan juga alamat IP pengunjung dan string agen pengguna browser untuk membantu deteksi spam.

  Kami menyimpan data formulir pengguna Pro berupa: Nama lengkap, Surel (email) dan Akun telegram. Hal ini bertujuan untuk memudahkan panggilan bantuan instalasi dan seputar masalah sistem operasi.

  ##### Komentar

  Kami tidak menggunakan layanan komentar pihak ketiga. 

  ##### Media

  Jika Anda mengunggah gambar ke situs web, Anda harus menghindari mengunggah gambar dengan data lokasi tertanam (GPS EXIF) yang disertakan. Pengunjung ke situs web dapat mengunduh dan mengekstrak data lokasi apa pun dari gambar di situs web.

  ##### Cookies dan peramban web

  Jika Anda meninggalkan formulir di situs kami, Anda dapat memilih untuk menyimpan nama, alamat email, dan situs web Anda dalam cookie. Ini untuk kenyamanan Anda sehingga Anda tidak perlu mengisi detail Anda lagi ketika Anda meninggalkan formulir lain. Cookie ini akan bertahan selama satu tahun.

  ##### Konten yang tersemat

  Artikel di situs ini dapat menyertakan konten yang disematkan (misalnya Video, gambar, artikel, dll.). Konten yang disematkan dari situs web lain berperilaku dengan cara yang persis sama seolah-olah pengunjung telah mengunjungi situs web lain. Situs web ini dapat mengumpulkan data tentang Anda dan menggunakan cookie.

### Mengontrol privasi Anda

Perhatikan bahwa Anda dapat mengubah pengaturan browser Anda untuk menonaktifkan cookies jika Anda menginginkannya. Menonaktifkan cookies untuk semua situs tidak dianjurkan karena dapat mengganggu penggunaan beberapa situs.

Pilihan terbaik adalah dengan menonaktifkan atau mengaktifkan cookies untuk situs tertentu yang Anda inginkan. Periksa dokumentasi browser Anda untuk instruksi bagaimana cara memblokir cookies dan mekanisme pelacakan lainnya.

### Analitik

Kami tidak menggunakan situs analitik.

### Berbagi data formulir

Kami hanya berbagi dengan data formulir dengan layanan CI/CD netlify.com. Lihat https://www.netlify.com/products/forms/.

### Keamanan

Situs ini adalah bagian dari jaringan situs yang melindungi terhadap serangan brute force yang terdistribusi. Untuk mengaktifkan perlindungan ini, alamat IP pengunjung yang mencoba masuk ke situs dibagikan dengan layanan yang disediakan oleh layanan netlify sendiri. Lihat https://www.netlify.com/security/.

### Informasi kontak

Jika Anda memiliki pertanyaan tentang kebijakan privasi langitketujuh.id maka Anda dapat menghubungi kami melalui halaman [Kabari](../kabari) yang disediakan atau menghubungi melalui email langsung di [langitketujuh.id@pm.me](mailto:langitketujuh.id@pm.me).

### Bagaimana kami melindungi data Anda

Kami tidak mengambil data pribadi anda kecuali alamat email di kolom formulir yang anda kirimkan secara sukarela oleh pengguna. Dan kami tidak menggunakan data tersebut untuk tujuan komersil atau promosi.
