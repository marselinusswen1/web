---
title: "Kabari Kami"
subtitle: ""
# meta description
description: "Hubungi kami jika ada saran, kritikan, dan memberikan pelaporan kutu. Atau sekedar menyapa kami :)"
image: media/beranda/thumbnail.jpg
opengraph:
  image: media/beranda/thumbnail.jpg
draft: false
---

### Butuh bantuan?
Terima kasih telah memberikan kritik dan saran kepada tim Langitketujuh OS. Anda juga dapat menyapa tim L7 melalui aplikasi telegram dengan tautan dibawah ini. Syukran.

* Layanan: [@LangitKetujuh_bot](https://t.me/LangitKetujuh_bot)
* Kanal: [@langitketujuhID](https://t.me/langitketujuhID)
* Surel: [langitketujuh.id@pm.me](mailto:langitketujuh.id@pm.me)
