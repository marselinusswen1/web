---
title: "Saran dan Kritikan!"
subtitle: ""
# meta description
description: "Terima kasih telah memilih LangitKetujuh OS. Mohon luangkan waktunya sekitar 5 menit untuk mengisi halaman feedback demi pengembangan kami selanjutnya. ^^"
image: media/beranda/thumbnail.jpg
opengraph:
  image: media/beranda/thumbnail.jpg
draft: false
---

### Feed Back!
Terima kasih, Anda telah memberikan saran dan kritikan kepada tim Langitketujuh ID.
Syukran.

* Layanan: [@LangitKetujuh_bot](https://t.me/LangitKetujuh_bot)
* Kanal: [@langitketujuhID](https://t.me/langitketujuhID)
* Surel: [langitketujuh.id@pm.me](mailto:langitketujuh.id@pm.me)
