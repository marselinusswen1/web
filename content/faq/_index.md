---
title: Pertanyaan dan Jawaban
subtitle: ''
description: Beberapa pertanyaan dan penjelasannya. Bisa jadi, apa yang Anda maksud
  ada disini.
image: media/beranda/thumbnail.jpg
opengraph:
  image: media/beranda/thumbnail.jpg

---
{{< faq "Berapa donasi yang harus dibayar?" >}}
Anda dapat memberikan [donasi](../donasi) sesuai kualitas pelayanan kami. Asalkan ikhlas, berkah dan tidak merugikan kedua belah pihak. [Donasi](../donasi) yang diberikan sesuai kemampuan Anda dan tidak ada beban. Tentunya kami sebagai tim pengembang projek hanyalah mencari keberkahan dalam membuat produk sistem operasi ini.
{{</ faq >}}

{{< faq "Bagaimana cara saya berdonasi?" >}}
Anda bisa [berdonasi](../donasi) dengan munggunakan Scan QR Code Qris. Kami menggunakan layanan pihak [Qris ID](https://qris.id) dengan NMID ID1021067117882 dari InterActive untuk kemudahkan transfer donasi. Pengiriman donasi bisa melalui aplikasi E-Wallet seperti GOPay, OVO, Dana, LinkAja dan aplikasi Mobile Banking lainnya.
{{</ faq >}}

{{< faq "Apakah saya perlu donasi dahulu?" >}} Jika Anda menggunakan [edisi lite](../lite) maka Anda bisa mendapatkannya secara gratis. Sedangkan untuk LangitKetujuh OS Profesional dengan layanan instalasi secara penuh, Anda harus mendonasikan mulai dari +Rp107.000. Donasi dapat dikembalikan apabila pemasangan tidak berhasil atau kami salurkan donasi tersebut ke [lazismu.org](https://lazismu.org/) dan [smart171.org](https://smart171.org) untuk mengirimkan bantuan dan kebutuhan Yatim di Gaza. {{</ faq >}}


{{< faq "Saya memerlukan bantuan tentang masalah teknis?" >}}
Anda bisa menggunakan [Layanan LangitKetujuh OS](https://t.me/LangitKetujuh_bot) via telegram. Jika menggunakan versi Lite tidak ada bantuan khusus tentang masalah teknis dan dukungan instalasi. Anda bisa melihat perbedaan fitur Langitketujuh di [halaman fitur](../fitur).
{{</ faq >}}

{{< faq "Apakah ada dokumentasi dan bantuan instalasi?" >}}
Dokumentasi langitketujuh yaitu [Panduan](https://panduan.langitketujuh.id/). Selengkapnya bisa mengunjungi [dokumentasi Voidlinux](https://docs.voidlinux.org/). Jika Anda mengunakan versi PRO, Anda dapat menghubungi tim [Layanan LangitKetujuh OS](https://t.me/LangitKetujuh_bot) untuk bantuan teknis.
{{</ faq >}}

{{< faq "Apakah musl tidak bisa menjalankan aplikasi Nonfree?" >}}
Tidak bisa langsung, karena aplikasi nonfree kebanyakan dibangun menggunakan glibc. Tetapi LangitKetujuh Musl mendukung aplikasi nonfree menggunakan flatpak seperti Zoom, Microsoft Teams, Spotify, Skype dan lain-lain. Selengkapnya ada disini ["Perbedaan musl dan glibc"](https://panduan.langitketujuh.id/perbandingan/musl-vs-glibc.html). Jika ada kesulitan Anda bisa menghubungi tim [Layanan LangitKetujuh OS](https://t.me/LangitKetujuh_bot).
{{</ faq >}}

{{< faq "Apakah musl tidak bisa menjalankan aplikasi Appimage?" >}}
Tidak bisa, karena aplikasi Appimage kebanyakan dibangun menggunakan glibc. Appimage hanya bisa dijalankan di Glibc. Anda bisa melihat perbedaan fitur Langitketujuh di [halaman fitur](../fitur).
{{</ faq >}}

{{< faq "Apakah saya boleh membagikan berkas `.iso` ke teman saya?" >}}
Ya boleh, tetapi untuk kepenggunaan personal saja dan tidak mengupload iso ke website atau media lainnya. Sebab, berkas `.iso` akan diperbarui jika ada rilis terbaru.
{{</ faq >}}

{{< faq "Apakah Langitketujuh OS bisa dipasang aplikasi wine?" >}}
Ya bisa. Musl hanya mendukung aplikasi windows 64-bit saja, sedangkan Glibc mendukung aplikasi windows 64-bit dan 32-bit.
{{</ faq >}}
