---
title: "Donasi Yuk!"
subtitle: ""
# meta description
description: "Dukung terus projek kami agar selalu bermanfaat yaa.."
image: media/beranda/thumbnail.jpg
opengraph:
  image: media/beranda/thumbnail.jpg
draft: false

call_to_action:
  enable : true
  title : "Scan QR Code ini!"
  image : "media/beranda/qrcode-langitketujuh-donasi.jpg"
  content : "Mendukung donasi dengan QRIS, Mobile Banking, E-Wallet seperti OVO, GoPay, LinkAja, DANA, Shopee Pay, WeChat Pay, Jenius dan E-wallet lainnya. (NMID: ID1021067117882)"
  button:
    enable : true
    label : "Kabari via Telegram"
    link : "https://t.me/langitketujuhCS"
---

### Motivasi kami yaitu berbagi dan memudahkan Anda untuk selalu berkarya.

Donasi bebas sesuai kemampuan dan ridho dari pengguna. Anda dapat memberikan donasi sesuai kualitas pelayanan kami, asalkan ikhlas, berkah dan tidak merugikan kedua belah pihak. Donasi yang diberikan sesuai kemampuan Anda dan tidak ada beban. Tentunya kami sebagai tim pengembang projek hanyalah mencari keberkahan dalam membuat produk sistem operasi ini.
Jazaakumullahu khairan katsiran. 🙏
