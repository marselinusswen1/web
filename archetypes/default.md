---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
image: media/blog/{{ .File.BaseFileName }}.webp
opengraph:
  image: media/blog/{{ .File.BaseFileName }}.webp
# post type (regular/featured)
type: "regular"
description: ""
author: "LangitKetujuh ID"
categories:
  - software
  - pattern
  - mockup
tag:
  - inkscape
  - gimp
  - krita
draft: true
---

_Barakallahu fiikum._
